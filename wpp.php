<?php 
	// Template Name: Redirecting Whatsapp
?>

<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" 
style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/01.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			Redirecionando...
		</h1>
	</div>
</div>

<script>
	setTimeout(function(){
	 window.location.href = 'https://api.whatsapp.com/send?phone=5511947031509';
         }, 5000);
</script>

<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>