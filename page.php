<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom opening-props" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			<?php the_title(); ?>
		</h1>
	</div>
	<div class="prop solid black" aria-hidden="true"></div>
</div>
<div class="center-content cleared about-props">
	<div class="prop solid red" aria-hidden="true"></div>
	<div class="prop transparent shadowed small">
		<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/00.jpg);"></div>
	</div>
	<main class="has-sidebar right">
		<?php 
		if (have_posts()) {
			while (have_posts()){
				the_post();
				?>
				<div class="wp-wrap">
					<?php the_content(); ?>
				</div>
				<?php 
			}
		}
		?>
	</main>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>