// generic helpers

var helpers = {
    lockBody : function()  {
        document.body.classList.add('no-scroll');
    },
    unlockBody : function() {
        document.body.classList.remove('no-scroll');
    },

    isMobile : window.innerWidth < 1025 ? true : false, 

    carousel : function(selector, options) {
        $(selector).slick(options);
    }, 

    carouselOptions : {
        opening : {
            infinite: true, 
            slidesToShow : 1, 
            fade: true,
            arrows: false, 
            dots: false,
            speed: 800,
            autoplay: true, 
            autoplaySpeed: 5000,
        }, 

        blog : {
            infinite : true, 
            arrows: false, 
            slidesToShow: 6,
            dots: false, 
            autoplay: true, 
            autoplaySpeed: 4000, 
            centerMode: true, 
            responsive: [
            {
                breakpoint: 1700,
                settings: {
                    slidesToShow: 4, 
                } 
            },
            {
                breakpoint: 1000, 
                settings: {
                    slidesToShow: 1,
                }
            }
            ], 
        }, 

        mobileBasic : {
            dots: false, 
            arrows: false, 
            slidesToShow: 1, 
        }, 

        product : {
            dots: false, 
            arrows: true, 
            infinite: true,
            adaptiveHeight: true,
        }
    }
}

// all header-related functions

var header = {
    scrollChange : function() {
        if (!helpers.isMobile) {
            var $menu = $('.skews'), 
            $still = $('.still'), 
            $fixed = $('.fixed');

            window.addEventListener('scroll', function() {

                var y = window.pageYOffset;

                if (y > 100) {
                    $still.addClass('up');
                    $fixed.addClass('down');
                }

                else {
                    $still.removeClass('up');
                    $fixed.removeClass('down');
                }
            });
        }
    },

    hamburger : function() {

        // open menu
        $('.hamburger').on('click', function(){
            if ( $(this).hasClass('is-active') ) {	
                $(this).removeClass('is-active');
                $('.header-links').removeClass('is-active');
                $('header .triangle').removeClass('is-active');
            }

            else {
                $(this).addClass('is-active')
                $('.header-links').addClass('is-active');
            }


            $('.return-submenu').removeClass('is-active');
            $('.mega-menu').removeClass('is-active');
        });


        // open submenu
        $('.has-sublinks').on('click', function() {
            $(this).addClass('is-active');
            $(this).children('.mega-menu').addClass('is-active');
            $('.return-submenu').addClass('is-active');
            $('header .triangle').addClass('is-active');
        });

        // close submenu
        $('.return-submenu').on('click', function(){
            var target = $('.mega-menu');
            if (!target.hasClass('is-active')) {
                target.addClass('is-active');
                $(this).addClass('is-active');
            }
            else {
                target.removeClass('is-active');
                $(this).removeClass('is-active')
                $('header .triangle').removeClass('is-active');
            }
        });
    }, 
}

// smooth anchors

function anchors() {
    $(".is-anchor").click(function(event) {     
        event.preventDefault();
        var target = $(this).data('to');

        $('html,body').animate({
            scrollTop: $(target).offset().top
        }, 500);
    });
}

// modal methods

function openModal(button, modal) {

    helpers.lockBody();

    button = $(button); 
    modal = $(modal);
    var persona; 


    button.on('click', function(e){
        var instance = $(this).data('instance'); 
        e.preventDefault();

        if (instance == '.we-call-form') {
            modal.addClass('call');
            persona = '.call';
        } 

        else if (instance == '.overlay .budget-request') {
            modal.addClass('budget');
        }

        else {
            modal.addClass('work');
            persona = '.work';
        }

        $('.overlay').fadeIn(400, function() {
            modal.fadeIn();
            $(instance).fadeIn();
            $(persona).fadeIn(500);
        });
    });

}

function closeModal(button, modal) {

    button = $(button); 
    modal = $(modal); 
    var persona = $('.modal-persona');

    button.on('click', function() { 

        $(modal).fadeOut(400, function() {
            $('.overlay').fadeOut();
        });

        setTimeout( function() {
            modal.removeClass('call work budget');
            persona.removeClass('call work');
        }, 400); 

    });

    helpers.unlockBody();
}

function checkForProductCarousel() {
    if ($('.full-product').children().length > 1) {
        helpers.carousel('.full-product', helpers.carouselOptions.product);
    }
}

// map method

var initMap = function() {
    var place = {lat: -23.479639, lng: -46.655779};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: place, 
        styles: mapStyles,
    });
    var marker = new google.maps.Marker({
        position: place,
        map: map,
    });	
}

// load listener

window.onload = function() {
    // hide loaders
    $('.overlay').fadeOut(400, function() {
        $('body').addClass('loaded');
    });
    

    // map 
    initMap();

    //carousel on products 
    checkForProductCarousel();

    // start carousel
    helpers.carousel('.fullscreen-blog-carousel', helpers.carouselOptions.blog);
    helpers.carousel('.opening-carousel', helpers.carouselOptions.opening);

    // start header 
    header.scrollChange();

    // instantiate modals
    openModal('.open-modal', '.modal-background'); 
    closeModal('.close-modal', '.modal-background');

    // instantiate anchors
    anchors();

    // mobile options
    if (helpers.isMobile) {
        helpers.carousel('.becomes-carousel-on-mobile', helpers.carouselOptions.mobileBasic);
        header.hamburger();
        setTimeout(function() {
            $('.zopim').hide();
        }, 500);
    }
}

