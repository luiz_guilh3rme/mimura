<?php get_header() ?>
<main class="structure">
	<section class="opening has-black-border-bottom opening-props">
		<div class="center-content">	
			<?php get_template_part('inc/budget-form'); ?>
		</div>
		<div class="opening-carousel">	
			<div class="opening-carousel-instance" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banners/00.jpg);">
				<div class="center-content">
					<h2 class="section-title white shaded">
						DIVISÓRIAS QUE DEIXAM 
						QUALQUER AMBIENTE MAIS 
						ORGANIZADO E FUNCIONAL
					</h2>
					<p class="generic-text white">
						A Mimura oferece o que há de melhor em divisórias de ambientes. Todas em materiais de qualidade, que podem ser de Madeira, Naval, Eucatex, Vidro, PVC, DryWall, MDF, Pallet e outros.
					</p>
					<a href="<?= site_url("/category/divisorias/")  ?>" class="btn-default red-and-black transitioned-basic" title="Ir para Divisórias">
						SAIBA MAIS
					</a>
				</div>
			</div>
			<div class="opening-carousel-instance" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banners/01.jpg);">
				<div class="center-content">
					<h2 class="section-title white shaded">
						FORROS DE ALTISSIMA
						QUALIDADE, COM MATERIAIS 
						RESISTENTES E DURÁVEIS
					</h2>
					<p class="generic-text white">
						Na Mimura você encontra forros de PVC, Madeira, Gesso, DryWall, Mineral, Colmeia, Lã em Vidro e Isopor. Todos confeccionados para atender a real necessidade do ambiente, escondendo fiações, encanamentos e imperfeições. 
					</p>
					<a href="<?= site_url("/category/forros/")  ?>" class="btn-default red-and-black transitioned-basic" title="Ir para Forros">
						SAIBA MAIS
					</a>
				</div>
			</div>
			<div class="opening-carousel-instance" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banners/02.jpg);">
				<div class="center-content">
					<h2 class="section-title white shaded">
						PERSIANAS QUE OFERECEM 
						ILUMINAÇÃO AGRADÁVEL 
						E RECINTO ELEGANTE
					</h2>
					<p class="generic-text white">
						A Mimura realiza a instalação de persianas horizontais, verticais, madeira ou alumínio, cada uma com funcionalidades específicas. Elas são duráveis, enobrecem o ambiente e elevam a qualidade da iluminação. 
					</p>
					<a href="<?= site_url("/category/persianas/")  ?>" class="btn-default red-and-black transitioned-basic" title="Ir para Persianas">
						SAIBA MAIS
					</a>
				</div>
			</div>
			<div class="opening-carousel-instance" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banners/03.jpg);">
				<div class="center-content">
					<h2 class="section-title white shaded">
						PISOS QUE SÃO UM
						EXCELENTE REVESTIMENTO
						E ALTAMENTE FUNCIONAIS
					</h2>
					<p class="generic-text white">
						Com a Mimura, o seu ambiente é revestido com pisos de excelente qualidade e funcionais. Temos pisos Laminados, Paviflex, Emborrachados, Vinílicos ou Frios, que oferecem vantagens diferentes para cada local.
					</p>
					<a href="<?= site_url("/category/pisos/")  ?>" class="btn-default red-and-black transitioned-basic">
						SAIBA MAIS
					</a>
				</div>
			</div>
		</div>
		<div class="prop solid black" aria-hidden="true"></div>
		<div class="prop solid red mobile" aria-hidden="true"></div>
	</section>
	<section class="about about-props" id="about-us">
		<div class="prop transparent shadowed small">
			<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/00.jpg);"></div>
		</div>
		<div class="prop solid red" aria-hidden="true"></div>
		<div class="prop transparent shadowed huge">
			<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/01.jpg);"></div>
		</div>
		<div class="center-content">	
			<h1 class="section-title red has-shaded-copy default-margin" data-shade="EXCELÊNCIA EM DIVISÓRIAS HÁ MAIS DE DUAS DÉCADAS">
				EXCELÊNCIA EM DIVISÓRIAS <br> HÁ MAIS DE DUAS DÉCADAS
			</h1>
			<p class="generic-text gray is-paragraph">
				Atuando há mais de 20 anos, a MIMURA segue sempre buscando diferenciar-se na qualidade de seu atendimento, oferecendo serviços em soluções arquitetônicas a seco que satisfaçam ou excedam as expectativas de seus diversos tipos de clientes.
			</p>
			<p class="generic-text gray is-paragraph">			
				Nossos profissionais são treinados e preparados para utilizar as técnicas adequadas na aplicação de nossos produtos para garantir que sua obra seja executada com segurança, eficiência e rapidez. Essa segurança oferece um tempo maior de vida útil e melhor resultado final em sua obra.
			</p>
			<p class="generic-text gray is-paragraph">					
				Seguindo os mais altos padrões de qualidade a MIMURA vem conquistando cada vez mais a satisfação de seus clientes, levando em seu portfólio um grande número de obras executadas com sucesso.
			</p>
			<p class="generic-text gray is-paragraph">					
				Nossos diversos produtos podem oferecer uma solução prática, estética, acústica ou térmica para seu ambiente.
			</p>
			<p class="generic-text gray is-paragraph">					
				Nós acreditamos que podemos fazer mais e melhor para atendê-lo com agilidade e excelência. Solicite uma visita técnica, que avaliaremos o local e indicaremos a melhor disposição e a forma mais econômica para suas divisórias.
			</p>
			<h2 class="section-subtitle red default-margin">
				CONHEÇA ALGUNS DE NOSSOS CLIENTES
			</h2>
			<div class="customers cleared becomes-carousel-on-mobile">
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/00.jpg" alt="Logotipo Quimicraft" title="Logotipo Quimicraft">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/01.jpg" alt="Logotipo Ipen" title="Logotipo Ipen">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/02.jpg" alt="Logotipo Fatec" title="Logotipo Fatec">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/03.jpg" alt="Logotipo Prefeitura da cidade de São Paulo" title="Logotipo Prefeitura da cidade de São Paulo">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/04.jpg" alt="Logotipo Imperial" title="Logotipo Imperial">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/05.jpg" alt="Logotipo Sempre" title="Logotipo Sempre">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/06.jpg" alt="Logotipo Nitro Química" title="Logotipo Nitro Química">
				</div>
				<div class="customer-instance">
					<img class="customer-image transitioned-basic" src="<?php bloginfo('template_url') ?>/images/customers/07.jpg" alt="Logotipo USP" title="Logotipo USP">
				</div>
			</div>
			<div class="mobile-carousel-orientation generic-text">
				<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
				<span>Deslize para ver mais. </span>
				<i class="fa fa-angle-double-right" aria-hidden="true"></i>
			</div>
		</div>
		<img src="<?php bloginfo('template_url'); ?>/images/common/persona2.png" aria-hidden="true" title="Mimura Divisórias"class="woman">
		<div class="persona-mobile" aria-hidden="true"></div>
	</section>
	<section class="types">
		<div class="center-content">
			<h2 class="section-title red has-shaded-copy default-margin" data-shade="TIPOS DE DIVISÓRIAS">
				TIPOS DE<br> DIVISÓRIAS
			</h2>
			<p class="generic-text gray">
				Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
			</p>
		</div>
		<?php get_template_part('inc/pictogram'); ?>
		<div class="mobile-carousel-orientation generic-text">
			<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
			<span>Deslize para ver mais. </span>
			<i class="fa fa-angle-double-right" aria-hidden="true"></i>
		</div>
	</section>
	<?php get_template_part('inc/blog-preview'); ?>
	<?php get_template_part('inc/contact-form') ?>
	<?php get_template_part('inc/map'); ?>
</main>
<?php get_footer(); ?>