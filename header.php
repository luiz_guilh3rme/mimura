<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php the_field('meta_title'); ?> | Mimura Construção de Ambientes</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="format-detection" content="telephone=no">
	<meta name="language" content="pt-br">
	<meta name="author" content="Agência 3xceler">
	<link rel="canonical" href="<?php the_field('meta_canonical'); ?>" />
	<meta name="description" content="<?php the_field('meta_description'); ?>">
	<meta name="language" content="pt-br" />
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/dist/main.css" type="text/css">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_url') ?>/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php bloginfo('template_url') ?>/favicon/safari-pinned-tab.svg" color="#ff000f">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#161616">
	<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121112366-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121112366-1');
</script>

</head>
<body>
	<div class="oldiewarning" aria-hidden="true" hidden="true">
		<a href="https://www.google.com.br/chrome/browser/desktop/" aria-hidden="true">
			POR FAVOR, CLIQUE AQUI E ATUALIZE SEU NAVEGADOR PARA ACESSAR O SITE.
		</a>
	</div>
	<header class="skews">
		<?php get_template_part('inc/header-still'); ?>
		<?php get_template_part('inc/header-fixed'); ?>
		<button class="hamburger hamburger--emphatic" type="button" aria-label="Abrir Menu">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
		<button class="return-submenu" aria-label="Fechar Submenu">VOLTAR</button>
	</header>
	<?php get_template_part('inc/modal'); ?>
