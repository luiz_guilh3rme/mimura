<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom opening-props" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
	<div class="center-content">
		<?php 
		$id = $post->ID;
		$get_cat = get_the_category($id); 
		?>
		<h1 class="section-title white smaller-margin">
			<?php 
			echo get_the_title();
			?>
		</h1>
		<p class="generic-text white slightly-bigger">
			<?php 
			echo $get_cat[0]->category_description;
			?>
		</p>
	</div>
	<div class="prop solid black" aria-hidden="true"></div>
</div>
<div class="center-content cleared about-props">
	<div class="prop solid red" aria-hidden="true"></div>
	<div class="prop transparent shadowed small">
		<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/00.jpg);"></div>
	</div>
	<main class="has-sidebar right">
		<div class="full-product blog-single">
			<?php 
			while ( have_posts() ) :
				the_post();
				if (get_the_post_thumbnail()) {
					?>
					<img src="<?php echo get_the_post_thumbnail_url('', 'product-image-full'); ?>"
					alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
					title="<?php echo get_the_post_thumbnail_caption(); ?>" 
					class="post-thumbnail">
					<?php
				} 
				else {
					?>
					<img src="<?php bloginfo('template_url') ?>/images/placeholders/product-image-full.png" alt="Imagem base cinza">
					<?php 
				}
				?>
			</div>
			<div class="wp-wrap">
				<?php the_content(); ?>
			</div>
			<?php 
		endwhile;
		?>
	</main>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>