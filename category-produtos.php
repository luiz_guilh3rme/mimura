<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			<?= get_cat_name(); ?>
		</h1>
		<p class="generic-text white slightly-bigger">
			<?= category_description(); ?>
		</p>
	</div>
</div>
<div class="center-content cleared">
	<main class="has-sidebar right">
		<div class="products cleared">
			<?php 
			$args = array(
				'posts_per_page' => 12, 
				'post_type' => 'produto',
			); 
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<div class="product one-third">
						<p class="product-title"><?php the_title(); ?></p>
						<div class="image-wrapper">
							<?php 
							if ( get_the_post_thumbnail() ) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'product-image'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="<?php bloginfo('template_url') ?>/images/placeholders/product-image.png" alt="Imagem base cinza">
								<?php 
							}
							?>
						</div>
						<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
					</div>
					<?php 
				}
			}
			?>
		</div>
	</main>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<section class="post-products-description">
	<div class="center-content">
		<p class="generic-text is-paragraph gray">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
		<p class="generic-text gray">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
	</div>
</section>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>