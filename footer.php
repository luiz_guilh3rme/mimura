<footer class="red-stripe-top">
    <div class="stripe red centered-text has-low-margin">
        <p class="generic-text white smaller-lh">
            <b>Rua Carolina Roque, 65 - Imirim | São Paulo - SP</b>
        </p>
    </div>
    <div class="center-content cleared">
        <?php get_template_part('inc/footer-menus'); ?>
    </div>
    <div class="stripe copyright black">
        <div class="center-content cleared">	
            <p class="generic-text smaller white">
                © <?php echo date('Y') ?> - Mimura Decorações. Todos os Direitos Reservados.
            </p>
            <div class="signature">	
                <a href="https://www.3xceler.com.br/criacao-de-sites" target="_BLANK" title="Agência 3xceler" class="signature-link transitioned-basic">
                    Criação de Sites
                </a>
                <span class="sr-only"> 
                    3xceler
                </span>
                <img src="<?php bloginfo('template_url'); ?>/images/common/3xceler.png" alt="Logotipo Agência 3xceler" class="3xceler">
            </div>
        </div>
    </div>
</footer>
<nav class="fixed-bottom-menu">
    <a href="wpp" class="cta-bottom dib"><i class="fa fa-whatsapp"></i>WHATSAPP</a>
    <a class="cta-bottom dib open-modal" data-instance=".overlay .budget-request" onclick="ga('gtag_UA_121112366_1.send', 'event','click', 'Formulário', 'Orçamento - Abriu')">ORÇAMENTO</a>
</nav>
<a href="wpp" target="_BLANK" class="whatsapp-cta transitioned-basic" title="Fale conosco no WhatsApp!">
<img src="<?php bloginfo('template_url') ?>/images/common/zapmimura.png" alt="Whatsapp" title="Whatsapp">
</a>
<script src="https://use.fontawesome.com/6e0b02b849.js"></script>
<script src="<?php bloginfo('template_url') ?>/dist/app.js"></script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuEXEx5_DcAdEZuVaxYxtG1s2gLTjlx0M&callback=initMap">
</script>
<script async defer type="text/javascript" src="https://phonetrack-static.s3.sa-east-1.amazonaws.com/68c694de94e6c110f42e587e8e48d852.js" id="script-pht-phone" data-cookiedays="5"></script>

<?php wp_footer(); ?>


<!-- Marcação JSON-LD gerada pelo Assistente de Marcação para Dados Estruturados Google. -->



<script type="application/ld+json">{
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "name": "Mimura Comercial e Serviços",
    "image": "http://www.mimura.com.br/wp-content/themes/mimura/images/common/logo2.png",
    "url": "https://www.mimura.com.br/",
    "telephone": [ "11 3981-2093", "11 3981-2235", "11 3985-7736" ],
    "address": {
    "@type": "PostalAddress",
    "streetAddress": "Rua Carolina Roque, 65 - Imirim | São Paulo - SP",
    "addressLocality": "São Paulo",
    "postalCode": "02472-030",
    "addressCountry": "Brazil",
    "addressRegion": "São Paulo"
},
"location": {
"@type": "Place",
"geo": {
"@type": "GeoCircle",
"geoRadius": "0"
}
},
"openingHours": [
"Mo-Fr 08:00-18:00"
],
"priceRange": "9999,99"
}</script> 


<script type="application/ld+json">{
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Mimura Comercial e Serviços",
    "url": "https://www.mimura.com.br/",
    "logo": "http://www.mimura.com.br/wp-content/themes/mimura/images/common/logo2.png"
}</script> 


<script type="application/ld+json">{
    "@context": "http://schema.org",
    "@type": "Person",
    "name": "Mimura Comercial e Serviços",
    "url": "https://www.mimura.com.br/",
    "homeLocation": {
    "@type": "Place",
    "address": {
    "@type": "PostalAddress",
    "addressCountry": "Brazil"
}
}
}</script> 


<script type="application/ld+json">{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "name": "Mimura Comercial e Serviços",
    "alternateName": "Mimura Comercial e Serviços",
    "url": "https://www.mimura.com.br/"
}</script> 
<script type="application/ld+json">
    {
       "@context": "http://schema.org",
       "@type": "BreadcrumbList",
       "itemListElement":
       [
       {
         "@type": "ListItem",
         "position": 1,
         "item":
         {
            "@id": "https://www.mimura.com.br",
            "name": "Dresses"
        }
    }
    ]
}
</script>


</body>
</html> 
