<?php 
$objID = get_queried_object()->ID;
$cat = get_the_category($objID);
get_header();
while ( have_posts() ) :
    the_post();
    ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
    <div class="center-content">
        <h2 class="section-title white smaller-margin shadowed">
            <?php the_category( '', '', false ); ?>
        </h2>
        <?php
            if ( get_field('description_excerpt') ) {  
                ?>
        <h1 class="generic-text white slightly-bigger">
            <?= 
                    get_field('description_excerpt');
                    ?>
        </h1>
        <?php 
            }
            ?>
    </div>
</div>
<div class="center-content cleared">
    <h1 class="product-full-title">
        <?php the_title(); ?>
    </h1>
    <?php 
        if ( get_field('product_highlight') ) {
            ?>
    <p class="product-excerpt generic-text gray bigger-paragraph">
        <?= get_field('product_highlight') ?>
    </p>
    <?php 
        }
        ?>
    <main class="has-sidebar right">
        <div class="full-product">
            <?php 
            $count = 0;
                if ( have_rows('gallery') ) : 
                    while(have_rows('gallery')) : 
                        $count++;
                        the_row(); 
                        if (get_the_post_thumbnail() && $count <= 1) {
                            the_post_thumbnail('product-image-full', ['class' => 'post-thumbnail']);
                        }
                        $img = get_sub_field('gallery_image'); 
                        $sizeURL = $img['sizes']['product-image-full'];
                        $caption = $img['caption'];
                        ?>
            <img src="<?= $sizeURL; ?>" alt="<?= $caption ?>">
            <?php 
            endwhile; 
            else : 
                if (get_the_post_thumbnail()) {
                    the_post_thumbnail('product-image-full', ['class' => 'post-thumbnail']);
                }
                else {
                    echo '<img src="www.placehold.it/800x500">'; 
                }
            endif;
            ?>
        </div>
        <div class="wp-wrap" style="margin-top: 50px;">
            <?php the_content(); ?>
        </div>
        <?php 
        endwhile;
        wp_reset_postdata();
        ?>
    </main>
    <?php get_template_part('inc/sidebar'); ?>
</div>
<?php get_template_part('inc/product-pictogram'); ?>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>