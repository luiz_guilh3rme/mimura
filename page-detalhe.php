<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			DIVISÓRIAS DE<br> AMBIENTE
		</h1>
		<p class="generic-text white slightly-bigger">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
	</div>
</div>
<div class="center-content cleared">
	<h2 class="section-subtitle red smaller-margin">
		BIOMBOS PARA ESCRITÓRIOS
	</h2>
	<p class="generic-text slightly-bigger is-paragraph gray"></p>
</div>
<div class="center-content cleared">
	<main class="has-sidebar right">
		<div class="full-product">
			<img src="http://placehold.it/800x500" alt="" class="full-product-image">
		</div>
	</main>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<section class="fullscreen-carousel-wrapper">
	<div class="fullscreen-carousel intern"></div>
</section>
<section class="post-detail-text">
	<div class="center-content">
		<p class="generic-text gray is-paragraph">
			As divisórias quando bem estruturadas facilitam a concentração no desenvolvimento de tarefas da equipe, por dividirem o espaço elas ajudam a evitar distrações. Em espaços compactos os biombos para escritório trazem a possibilidade de criar diversos ambientes funcionais, tornando o seu espaço corporativo funcional e confortável.
		</p>
		<p class="generic-text gray is-paragraph">
			A grande qualidade que se obtém em apostar em biombos como elemento de decoração em destaque na sua propriedade é a versatilidade. Nada restritivo, o objeto em si não lhe obriga à realização de troca e posicionamento dos móveis.
		</p>
		<h2 class="section-subtitle red smaller-margin">
			TOQUE INUSITADO
		</h2>
		<p class="generic-text gray is-paragraph">
			Alguns cuidados fazem toda diferença na hora de montar um ambiente, entãoé importante dar atenção aos detalhes. Por exemplo, biombos vazados, com venezianas, dão a sensação de profundidade, enquanto os espelhados dão a sensação de cômodos ampliados. 
		</p>
		<p class="generic-text gray">
			Como criatividade e decoração são sinônimos de ousadia, por que não posicionar os biombos em lugares inusitados, apenas para incrementar esses ambientes quando tem peças de apelo estético? Usados como painéis, ou colocados atrás de outros móveis ou sofás e aparadores. 
		</p>
	</div>
</section>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_footer(); ?>