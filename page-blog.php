<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/01.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			BLOG<br> MIMURA
		</h1>
		<p class="generic-text white slightly-bigger">
			Acesse o nosso blog e confira novidades e informações sobre nossos produtos e também dicas como otimizar seu espaço. Clique aqui e saiba mais!
		</p>
	</div>
</div>
<div class="center-content cleared">
	<main class="has-sidebar right">
		<div class="blog-full-feed becomes-carousel-on-mobile">
			<?php 
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page' => 6, 
				'post-type' => 'post',
				'paged' => $paged, 
				'post__not_in'  => array(5, 12, 15),
			); 
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<article class="blog-post-instance half">
						<h2 class="blog-post-title">
							<?php the_title() ?>
						</h2>
						<p class="blog-post-date">
							<i class="fa fa-calendar"></i>
							<span class="dib generic-text gray smallest"><?php the_time('d, F, Y') ?></span>
						</p>
						<div class="image-wrapper">
							<?php 
							if (get_the_post_thumbnail()) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'blog-preview'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="<?php bloginfo('template_url') ?>/images/placeholders/blog-preview.png" alt="Imagem base cinza">
								<?php 
							}
							?>
						</div>
						<a href="<?php the_permalink(); ?>" class="go-to-blog-post btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
					</article>
					<?php 
				}
				wp_reset_postdata();
			}
			wp_pagenavi( array( 'query' => $query ) );
			?>
		</div>
		<div class="mobile-carousel-orientation generic-text">
			<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
			<span>Deslize para ver mais. </span>
			<i class="fa fa-angle-double-right" aria-hidden="true"></i>
		</div>
	</main>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<section class="fullscreen-carousel-wrapper">
	<?php get_template_part('inc/pictogram'); ?>
</section>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>