<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom opening-props" style="background-image: url('<?php the_field('category_image'); ?>');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin shaded">
			<?php 
			$obj = get_queried_object(); 
			echo $obj->name;
			?>
		</h1>
		<?php
		if ( get_field('description_excerpt') ) {  
			?>
			<p class="generic-text white slightly-bigger">
				<?= 
				get_field('description_excerpt');
				?>
			</p>
			<?php 
		}
		?>
	</div>
	<div class="prop solid black" aria-hidden="true"></div>
</div>
<div class="center-content cleared about-props">
	<div class="prop solid red" aria-hidden="true"></div>
	<div class="prop transparent shadowed small">
		<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/00.jpg);"></div>
	</div>
	<main class="has-sidebar right">
		<div class="products becomes-carousel-on-mobile cleared">
			<?php 
			$id = $get_cat[0]->cat_ID;
			$args = array(
				'posts_per_page' => 12,	
				'post_type' => 'produtos',
				'cat' => $obj->cat_ID,
			); 
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<div class="product one-third">
						<p class="product-title"><?php the_title(); ?></p>
						<div class="image-wrapper">
							<?php 
							if ( get_the_post_thumbnail() ) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'product-image'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="<?php bloginfo('template_url') ?>/images/placeholders/product-image.png" alt="Imagem base cinza">
								<?php 
							}
							?>
						</div>
						<a href="<?php the_permalink(); ?>" class="go-to-product-page btn-default full-red transitioned-basic shaded" title="Ir Para <?php the_title(); ?>" >SAIBA MAIS</a>
					</div>
					<?php 
				}
			}
			wp_reset_postdata();
			?>
		</div>
	</main>
	<div class="mobile-carousel-orientation generic-text">
		<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
		<span>Deslize para ver mais. </span>
		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
	</div>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<section class="post-products-description wp-wrap">
	<div class="center-content">
		<p class="generic-text gray">
			<?= 
			$obj->description;
			?>
		</p>
	</div>
</section>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>