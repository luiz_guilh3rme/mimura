<?php

// thumbnails and sizes
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'blog-preview', '400', '200', true );
	add_image_size( 'product-image', '300', '200', true );
  add_image_size('workplace-spot', '400', '220', true);
  add_image_size( 'product-image-full', '800', '500', true );
  add_image_size( 'product-pictogram', '460', '300', true );
  add_image_size( 'breadcrumb-category', '1920', '400', true );
}

// removing useless boxes on admin panel

function remove_boxes() {	
	remove_meta_box( 'tagsdiv-post_tag', 'post', 'normal' );
}

// insert font awesome element before wp_list_categories li 

function i_before_link_list_categories( $list ) {
	$list = str_replace('<a href=','<i class="fa fa-angle-double-right"></i><a href=',$list);
	return $list;
}

// post types 
function create_posttype() {
  register_post_type( 'produtos',
    array(
      'labels' => array(
        'name' => __( 'Produtos' ),
        'singular_name' => __( 'Produto' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array(
       'slug' => 'produtos',
       'with_front' => false
     ),
      'supports' => array('title','editor','thumbnail'),
      'taxonomies'  => array( 'category' ),
    )
  );

  register_post_type( 'Vagas',
    array(
      'labels' => array(
        'name' => __( 'Vagas' ),
        'singular_name' => __( 'Vaga' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array(
        'slug' => 'vagas',
        'with_front'  => false,
      ),
      'supports' => array('title','editor','thumbnail'),
    )
  );

  register_post_type( 'servicos',
    array(
      'labels' => array(
        'name' => __( 'Serviços' ),
        'singular_name' => __( 'Serviço' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array(
        'slug' => 'servicos',
        'with_front'  => false,
      ),
      'supports' => array('title','editor','thumbnail'),
    )
  );
  
}

function filter_head() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}

// actions / filters 
add_action('get_header', 'filter_head');
add_action( 'init', 'create_posttype' );
add_action( 'admin_menu', 'remove_boxes');
add_filter ( 'wp_list_categories', 'i_before_link_list_categories' );


// remove /produtos/ from slug 

function na_remove_slug( $post_link, $post, $leavename ) {

    if ( 'produtos' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}

add_filter( 'post_type_link', 'na_remove_slug', 10, 3 );

function na_parse_request( $query ) {

    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'produtos', 'page' ) );
    }
}
add_action( 'pre_get_posts', 'na_parse_request' );