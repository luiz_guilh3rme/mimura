<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/00.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			DIVISÓRIAS DE<br> AMBIENTE
		</h1>
		<p class="generic-text white slightly-bigger">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
	</div>
</div>
<div class="center-content cleared">
	<main class="has-sidebar right">
		<div class="products cleared becomes-carousel-on-mobile">
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
			<div class="product one-third">
				<p class="product-title">What the Fucking Ever</p>
				<div class="image-wrapper">
					<img src="http://placehold.it/300x200" alt="">
				</div>
				<a href="" class="go-to-product-page btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
			</div>
		</div>
	</main>
	<div class="mobile-carousel-orientation generic-text">
		<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
		<span>Deslize para ver mais. </span>
		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
	</div>
	<?php get_template_part('inc/sidebar'); ?>
</div>
<section class="post-products-description">
	<div class="center-content">
		<p class="generic-text is-paragraph gray">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
		<p class="generic-text gray">
			Indicadas para o planejamento de salas e escritórios, com soluções que se adaptam perfeitamente a todos os tipos de projetos, dominam há décadas o mercado. Para quem necessita de urgência na montagem e praticidade, sem dores de cabeça. As divisórias são encaixadas e fixadas em perfis de aço naval de acordo com o projeto do cliente. Podendo ser colocados módulos de vidro liso ou plus, portas em Eucatex e de correr. Com disponibilidade de painéis com 35 e 48mm e cores variadas.
		</p>
	</div>
</section>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_footer(); ?>