<nav class="fixed">
	<div class="center-content cleared not-relative">
		<a href="<?= site_url('/'); ?>" class="logo-small">
			<img src="<?php bloginfo('template_url'); ?>/images/common/logo2.png" alt="Logotipo Mimura">
		</a>
		<ul class="header-links">
			<li class="link dib">
				<a href="<?=site_url('/'); ?>" title="Home">
					HOME
				</a>
			</li>
			<li class="link dib has-sublinks nolowborder">
				<a>
					PRODUTOS
				</a>
				<div class="mega-menu">	
					<div class="center-content nopadding">
						<div class="menu-column">	
							<p class="duplicate-title shaded">
								PRODUTOS
							</p>
						</div>
						<div class="menu-column">
							<ul class="sublinks">
								<li class="is-category">
									<a title="Ir para Divisórias" class="transitioned-basic" href="<?= site_url('/category/divisorias/') ?>">Divisórias</a>
								</li>
								<li><a title="Ir para Divisória Naval" class="transitioned-basic" href="<?= site_url('/divisoria-naval/') ?>">Divisória Naval</a></li>
								
								<li><a title="Ir para Divisória de Vidro" class="transitioned-basic" href="<?= site_url('/divisoria-de-vidro/') ?>">Divisória de Vidro</a></li>
								<li><a title="Ir para Divisória de DryWall" class="transitioned-basic" href="<?= site_url('/divisoria-de-drywall/') ?>">Divisória de DryWall</a></li>
							</ul>
						</div>
						<div class="menu-column">
							<ul class="sublinks">
								<li class="is-category">
									<a title="Ir para Forros" class="transitioned-basic" href="<?= site_url('/category/forros/') ?>">Forros</a>
								</li>
								<li><a title="Ir para Forro de PVC" class="transitioned-basic" href="<?= site_url('/forro-de-pvc/') ?>">Forro de PVC</a></li>
								<li><a title="Ir para Forro Mineral" class="transitioned-basic" href="<?= site_url('/forro-mineral/') ?>">Forro Mineral</a></li>
								<li><a title="Ir para Forro em Lã de Vidro" class="transitioned-basic" href="<?= site_url('/forro-em-la-de-vidro/') ?>">Forro em Lã de Vidro</a></li>
								<li><a title="Ir para Forro de Isopor" class="transitioned-basic" href="<?= site_url('/forro-de-isopor/') ?>">Forro de Isopor</a></li>
								<li><a title="Ir para Forro de DryWall" class="transitioned-basic" href="<?= site_url('/forro-de-drywall/') ?>">Forro de DryWall</a></li>
							</ul>
						</div>
                        <div class="menu-column">
							<ul class="sublinks">
                                <li class="is-category">
									<a class="transitioned-basic">Mezaninos</a>
								</li>
								<li><a title="Ir para Mezaninos Metálicos" class="transitioned-basic" href="<?= site_url('/mezaninos-metalicos/') ?>">Mezaninos Metálicos</a></li>
								<!-- <li><a title="Ir para Mezaninos Metálicos" class="transitioned-basic" href="<?php  // echo site_url('/mezaninos-metalicos/') ?>">Escadas Metalica</a></li> -->
							</ul>
						</div>
						<div class="menu-column">
							<ul class="sublinks">
								<li class="is-category">
									<a title="Ir para Pisos" class="transitioned-basic" href="<?= site_url('/category/pisos/') ?>">Pisos</a>
								</li>
								<li><a title="Ir para Piso Laminados" class="transitioned-basic" href="<?= site_url('/pisos-laminados/') ?>">Pisos Laminados</a></li>
								<li><a title="Ir para Pisos Emborrachados" class="transitioned-basic" href="<?= site_url('/pisos-emborrachados/') ?>">Pisos Emborrachados</a></li>
								<li><a title="Ir para Pisos Vinilicos" class="transitioned-basic" href="<?= site_url('/pisos-vinilicos/') ?>">Pisos Vinilicos</a></li>
								<li><a title="Ir para Pisos Paviflex" class="transitioned-basic" href="<?= site_url('/pisos-paviflex/') ?>">Pisos Paviflex</a></li>
								<li><a title="Ir para Pisos Frios" class="transitioned-basic" href="<?= site_url('/pisos-frios/') ?>">Pisos Frios</a></li>
							</ul>
						</div>
						<div class="menu-column">
							<ul class="sublinks">
								<li class="is-category">
									<a title="Ir para Persianas" class="transitioned-basic" href="<?= site_url('/category/persianas/') ?>">Persianas</a>
								</li>
								<li><a title="Ir para Persianas Verticais" class="transitioned-basic" href="<?= site_url('/persianas-verticais/') ?>">Persianas Verticais</a></li>
								<li><a title="Ir para Persianas Horizantais" class="transitioned-basic" href="<?= site_url('/persianas-horizontais/') ?>">Persianas Horizantais</a></li>
								<li><a title="Ir para Persianas de Madeira" class="transitioned-basic" href="<?= site_url('/persianas-de-madeira/') ?>">Persianas de Madeira</a></li>	
								<li><a title="Ir para Persianas de Alumínio" class="transitioned-basic" href="<?= site_url('/persianas-de-aluminio/') ?>">Persianas de Alumínio</a></li>	
							</ul>
						</div>
					</div>
				</div>
			</li>
			<li class="link dib has-sublinks nolowborder">
				<a>
					SERVIÇOS
				</a>
				<div class="mega-menu">	
					<div class="center-content nopadding">
						<div class="menu-column">	
							<p class="duplicate-title shaded">
								SERVIÇOS
							</p>
						</div>
						<div class="menu-column">
							<ul class="sublinks">
                                <li class="is-category">
									<a class="transitioned-basic">Reformas</a>
								</li>
								<li>
									<a title="Ir para Reformas de Escritório" class="transitioned-basic" href="<?= site_url('/servicos/reformas-de-escritorio/'); ?>">Reformas de Escritório</a>
								</li>
								<li>
									<a title="Ir para Reformas Residenciais" class="transitioned-basic" href="<?= site_url('/servicos/reformas-residenciais/'); ?>">Reformas Residenciais</a>
								</li>
								<li>
									<a title="Ir para Pintura" class="transitioned-basic" href="<?= site_url('/servicos/pintura/'); ?>">Pintura</a>
								</li>
                                <li><a title="Ir para Bate Maca" class="transitioned-basic" href="<?= site_url('/bate-maca/') ?>">Bate Maca</a></li>
								<li><a title="Ir para Película de Proteção Solar" class="transitioned-basic" href="<?= site_url('/pelicula-de-protecao-solar/') ?>">Película de Proteção Solar</a></li>
							</ul>
						</div>
						<div class="image-column">
							<img src="<?php bloginfo('template_url'); ?>/images/props/04.jpg" alt="Close em cinto de ferramentas, martelo e capacete amarelo numa reforma">
						</div>
					</div>
				</div>
			</li>
			<li class="link dib">
				<?php if ( is_front_page() ) { ?>
					<a title="Sobre nós" class="is-anchor" data-to="#about-us">
						SOBRE NÓS
					</a>
					<?php 
				}
				else {
					?>
					<a href="<?= site_url('#about-us'); ?>" title="Sobre nós">
						SOBRE NÓS
					</a>
					<?php 
				}
				?>
			</li>
			<li class="link dib">
				<a href="<?= site_url('/trabalhe-conosco/'); ?>" title="Trabalhe Conosco">
					TRABALHE CONOSCO
				</a>
			</li>
			<li class="link dib">
				<a href="<?= site_url('/blog/'); ?>" title="Blog">
					BLOG
				</a>
			</li>
			<li class="link dib">
				<a title="Ir para seção de contato" class="is-anchor" data-to="#contact-section">
					CONTATO
				</a>
			</li>
			<li class="link dib has-schedule-button nolowborder">
				<button class="btn-default full-red fixed-header-cta transitioned-basic open-modal" 
				onclick="ga('gtag_UA_121112366_1.send', 'event','click','Formulário', 'Nós te Ligamos - Header')"
				data-instance=".we-call-form" title="Nós te Ligamos">
					NÓS TE LIGAMOS
				</button>
			</li>
			<li class="link phone-and-whatsapp">
				<a href="tel:1139812093" target="_BLANK" itemprop="telephone" title="Ligue para nós!">
					<i class="fa fa-phone" aria-hidden="true"></i>
					11 3981.2093
				</a>
				<a href="https://api.whatsapp.com/send?phone=5511947031509" 
				onclick="ga('gtag_UA_121112366_1.send', 'event', 'click', 'Whatsapp', 'Header')"
				target="_BLANK" title="Fale conosco no Whatsapp">
					<i class="fa fa-whatsapp" aria-hidden="true"></i>
					11 94703.1509
				</a>
			</li>
		</ul>
	</div>
</nav>