		<div class="fullscreen-carousel becomes-carousel-on-mobile">
			<?php
			$getCategory = get_the_category(get_queried_object()->ID)[0];
			if ( is_single( array( 116, 117,107 ) ) ) {
				$args = array(
					'posts_per_page' => 4,
					'post_type' => 'produtos',
					'orderby' => 'rand', 
					'post__not_in' => array(get_queried_object()->ID[0])
				); 
			}
			else {	
				$args = array(
					'posts_per_page' => 4,
					'post_type' => 'produtos',
					'cat' => $getCategory->cat_ID,
					'orderby' => 'rand', 
				); 
			}
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<a href="<?php the_permalink(); ?>" class="fullscreen-carousel-instance dib">
						<div class="overlay-triangle" aria-hidden="true">
						</div>
						<div class="overlay-triangle-information">
							<p class="product-name-triangle">
								<?php the_title(); ?>
							</p>
							<i class="fa fa-search" aria-hidden="true"></i>
						</div>
						<?php 
						if ( get_the_post_thumbnail() ) {
							?>
							<div class="grows" 
							style="background-image: url(<?= get_the_post_thumbnail_url('', 'product-pictogram') ?>); ">	
						</div>
						<?php
					} 
					else {
						?>
						<div class="grows" 
						style="background-image: url('http://placehold.it/460x300'); ">	
					</div>
					<?php 
				}
				?>
			</a>
			<?php
		}
	}
	wp_reset_postdata();
	?>
</div>