	<section class="blog-preview">
		<div class="center-content">
			<h2 class="section-title red has-shaded-copy default-margin" data-shade="ÚLTIMAS DO BLOG">
				ÚLTIMAS <br> DO BLOG
			</h2>
			<p class="generic-text gray bigger-paragraph">
				Ainda não sabe qual divisória escolher? Acesse o nosso blog e fique por dentro das novidades e informações sobre divisórias e outros produtos, qual o melhor material e como utilizar.
			</p>
		</div>
		<div class="fullscreen-blog-carousel">
			<?php 
			$args = array(
				'posts_per_page' => 12, 
				'post-type' => 'post',
			); 
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<article class="blog-post-instance">
                        <a href="<?php the_permalink(); ?>" title="Ir para <?php the_title(); ?>">
                            <p class="blog-post-title">
                                <?php the_title(); ?>
                            </p>
                        </a>
						<p class="blog-post-date">
							<i class="fa fa-calendar"></i>
							<span class="dib generic-text gray smallest"><?php the_time('d, F, Y') ?></span>
						</p>
						<div class="image-wrapper">
                            <a href="<?php the_permalink(); ?>" title="Ir para <?php the_title(); ?>">
							<?php 
							if (get_the_post_thumbnail()) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'blog-preview'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="<?php bloginfo('template_url') ?>/images/placeholders/blog-preview.png" alt="Imagem base cinza">
								<?php 
							}
							?>
                            </a>    
						</div>
                        
						<a href="<?php the_permalink(); ?>" class="go-to-blog-post btn-default full-red transitioned-basic shaded" title="Ir para <?php the_title(); ?>">SAIBA MAIS</a>
					</article>
					<?php 
				}
			}
			?>
		</div>
		<div class="mobile-carousel-orientation generic-text">
			<i class="fa fa-angle-double-left" aria-hidden="true"></i> 
			<span>Deslize para ver mais. </span>
			<i class="fa fa-angle-double-right" aria-hidden="true"></i>
		</div>
	</section>