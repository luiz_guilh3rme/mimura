<aside class="has-budget-form">
	<?php get_template_part('inc/budget-form-inner'); 
	if ( is_page( 'blog' ) || is_category() || $post->post_type != 'produtos' )  { 
		?>
		<div class="shadowed">
			<h3 class="sidebar-title">
				<i class="fa fa-angle-double-right"></i>
				ÚLTIMOS POSTS
			</h3>
			<?php 
			$args = array(
				'posts_per_page' => 3, 
				'post-type' => 'post',
				'post_not__in' => array($post->id),
			); 
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?>
					<a href="<?php the_permalink(); ?>" class="sidebar-preview" title="Ir para <?php the_title(); ?>">
						<div class="sidebar-image-wrapper">
							<?php 
							if (get_the_post_thumbnail()) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'blog-preview'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="<?php bloginfo('template_url') ?>/images/placeholders/blog-preview.png" alt="Imagem base cinza">
								<?php 
							}
							?>
						</div>
						<h4 class="sidebar-post-name">
							<?php the_title(); ?>
						</h4>
					</a>
					<?php 
				}
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="shadowed">
			<h3 class="sidebar-title">
				<i class="fa fa-angle-double-right"></i>
				CATEGORIAS
			</h3>
			<ul class="category-listing generic-text smaller">
				<?php wp_list_categories(
					array(
						'title_li' => '',
						'exclude' => 1,
					)
				); 
				?>
				<li>
					<a title="Ir para Reformas de Escritório" class="transitioned-basic" href="http://www.mimura.com.br/servicos/reformas-de-escritorio/">
						<i class="fa fa-angle-double-right"></i>
						Reformas de Escritório
					</a>
				</li>
				<li>
					<a title="Ir para Reformas Residenciais" class="transitioned-basic" href="http://www.mimura.com.br/servicos/reformas-residenciais/">
						<i class="fa fa-angle-double-right"></i>
						Reformas Residenciais
					</a>
				</li>
				<li>
					<a title="Ir para Pintura" class="transitioned-basic" href="http://www.mimura.com.br/servicos/pintura/">
						<i class="fa fa-angle-double-right"></i>
						Pintura
					</a>
				</li>
			</ul>	
		</div>
		<?php 
	} 
	?>
</aside>