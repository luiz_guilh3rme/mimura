		<div class="fullscreen-carousel becomes-carousel-on-mobile">
			<a href="<?= site_url('/produtos/divisoria-naval/') ?>" class="fullscreen-carousel-instance dib" title="Ir para Divisória Naval">
				<div class="overlay-triangle" aria-hidden="true">
				</div>
				<div class="overlay-triangle-information">
					<p class="product-name-triangle">
						DIVISÓRIA NAVAL
					</p>
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<div class="grows" style="background-image: url('wp-content/uploads/2018/06/div-naval.jpg');">	
				</div>
			</a>
			<a href="<?= site_url('/produtos/divisoria-de-eucatex/') ?>" class="fullscreen-carousel-instance dib" title="Ir para Divisória de Eucatex">
				<div class="overlay-triangle" aria-hidden="true">
				</div>
				<div class="overlay-triangle-information">
					<p class="product-name-triangle">
						DIVISÓRIA DE EUCATEX
					</p>
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<div class="grows" style="background-image: url('wp-content/uploads/2018/06/div-eucatex.jpg');">	
				</div>
			</a>
			<a href="<?= site_url('/produtos/divisoria-de-vidro/') ?>" class="fullscreen-carousel-instance dib" title="Ir para Divisória de Vidro">
				<div class="overlay-triangle" aria-hidden="true">
				</div>
				<div class="overlay-triangle-information">
					<p class="product-name-triangle">
						DIVISÓRIA DE VIDRO
					</p>
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<div class="grows" style="background-image: url('wp-content/uploads/2018/06/div-vidro.jpg');">	
				</div>
			</a>
			<a href="<?= site_url('/produtos/divisoria-de-drywall/') ?>" class="fullscreen-carousel-instance dib" title="Ir para Divisória de Drywall">
				<div class="overlay-triangle" aria-hidden="true">
				</div>
				<div class="overlay-triangle-information">
					<p class="product-name-triangle">
						DIVISÓRIAS DE DRYWALL
					</p>
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<div class="grows" style="background-image: url('wp-content/uploads/2018/06/div-drywall.jpg');">	
				</div>
			</a>
		</div>