<div class="footer-column one-fourth">
	<a href="<?php echo site_url('/'); ?>" class="logo">
		<img src="<?php bloginfo('template_url') ?>/images/common/logo.png" title="Logotipo Mimura Decorações" alt="Logotipo Mimura Decorações">
	</a>
		<?php 
		if ( is_front_page() ) 
		{ 
			?>
			<a title="Ir para seção Sobre Nós" data-to="#about-us" class="is-anchor footer-title bigger-margin">
				SOBRE NÓS
			</a>
			<?php 
		}
		else 
		{
			?>
			<a href="<?= site_url('#about-us'); ?>" class="footer-title bigger-margin" title="Ir para seção Sobre Nós">
				SOBRE NÓS
			</a>
			<?php 
		}
		?>
	<p class="generic-text white smaller">
		Atuando há mais de 20 anos, a MIMURA segue sempre buscando diferenciar-se na qualidade de seu atendimento, oferecendo serviços em soluções arquitetônicas a seco que satisfaçam ou excedam as expectativas de seus diversos tipos de clientes.
	</p>
</div>	
<div class="footer-column one-fourth">
	<a href="<?= site_url('/trabalhe-conosco/'); ?>" class="footer-title" title="Trabalhe Conosco">
		TRABALHE CONOSCO
	</a>
	<a href="<?= site_url('/blog/');  ?>" class="footer-title">
		BLOG
	</a>
	<?php 
	if 
		( is_front_page() ) 
	{ 
		?>
		<a title="Ir para seção de contato" class="is-anchor footer-title" data-to="#contact-section">
			CONTATO
		</a>
		<?php 
	}
	else {
		?>
		<a href="<?= site_url('#contact-section'); ?>" title="Ir para seção de contato" class="footer-title" >
			CONTATO
		</a>
		<?php 
	}
	?>
	<p class="generic-text smaller white is-paragraph">
		<i class="fa fa-phone"></i>
		Entre em contato
	</p>
	<p class="phone-instance">
		<a href="tel:1139812093" itemprop="telephone" target="_BLANK" title="Ligue para nós!">11 3981.2093</a>
	</p>
	<p class="phone-instance">
		<a href="tel:1139812235" itemprop="telephone" target="_BLANK" title="Ligue para nós!">11 3981.2235</a>
	</p>
	<p class="phone-instance">
		<a href="tel:1139857736" itemprop="telephone" target="_BLANK" title="Ligue para nós!">11 3985.7736</a>
	</p>
	<p class="generic-text white whatsapp-footer">
		<a href="wpp"
		onclick="ga('gtag_UA_121112366_1.send', 'event','click', 'Whatsapp', 'Footer')"
		 target="_BLANK" title="Fale conosco no Whatsapp">	
			<i class="fa fa-whatsapp"></i>
			<b>11 94703.1509</b>
		</a>
	</p>
	<p class="generic-text white email-footer">
		<i class="fa fa-envelope"></i>
		<a href="mailto:mimura@mimura.com.br"
		onclick="ga('gtag_UA_121112366_1.send', 'event','click','Email', 'Footer')"
		 title="Entre em contato conosco!">mimura@mimura.com.br</a>
	</p>
</div>
<div class="footer-column one-fourth">
	<p class="footer-title bigger-margin">
		PRODUTOS
	</p>
	<a title="Ir para Divisórias de Ambientes"  class="footer-subtitle" href="<?= site_url('/category/divisorias/') ?>">
		Divisórias de Ambiente
	</a>
	<ul class="footer-list">
		<li><a title="Ir para Divisória Naval" class="transitioned-basic" href="<?php echo site_url('/divisoria-naval/') ?>">Divisória Naval</a></li>
		<li><a title="Ir para Divisória de Eucatex" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-eucatex/') ?>">Divisória de Eucatex</a></li>
        <li><a title="Ir para Divisória de Vidro" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-vidro/') ?>">Divisória de Vidro</a></li>
        <li><a title="Ir para Divisória de DryWall" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-drywall/') ?>">Divisória de DryWall</a></li>
        <li><a title="Ir para Divisórias Usadas" class="transitioned-basic" href="<?php echo site_url('/divisorias-usadas/') ?>">Divisórias Usadas</a></li>
        <li><a title="Ir para Divisórias Cegas" class="transitioned-basic" href="<?php echo site_url('/divisorias-cegas/') ?>">Divisórias Cegas</a></li>
        <li><a title="Ir para Divisórias de Madeira" class="transitioned-basic" href="<?php echo site_url('/divisorias-de-madeira/') ?>">Divisórias de Madeira</a></li>
        <li><a title="Ir para Divisória de MDF" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-mdf/') ?>">Divisória de MDF</a></li>
        <li><a title="Ir para Divisória de PVC" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-pvc/') ?>">Divisória de PVC</a></li>
        <li><a title="Ir para Divisória de Pallet" class="transitioned-basic" href="<?php echo site_url('/divisoria-de-pallet/') ?>">Divisória de Pallet</a></li>
	</ul>
	<a  title="Ir para Forros" class="footer-subtitle"  href="<?= site_url('/category/forros/') ?>">
		Forros
	</a>
	<ul class="footer-list">
		<li><a title="Ir para Forro de PVC" class="transitioned-basic" href="<?php echo site_url('/forro-de-pvc/') ?>">Forro de PVC</a></li>
        <li><a title="Ir para Forro Mineral" class="transitioned-basic" href="<?php echo site_url('/forro-mineral/') ?>">Forro Mineral</a></li>
        <li><a title="Ir para Forro em Lã de Vidro" class="transitioned-basic" href="<?php echo site_url('/forro-em-la-de-vidro/') ?>">Forro em Lã de Vidro</a></li>
        <li><a title="Ir para Forro de Isopor" class="transitioned-basic" href="<?php echo site_url('/forro-de-isopor/') ?>">Forro de Isopor</a></li>
        <li><a title="Ir para Forro Em Colméia" class="transitioned-basic" href="<?php echo site_url('/forro-em-colmeia/') ?>">Forro Em Colméia</a></li>
        <li><a title="Ir para Forro de Madeira" class="transitioned-basic" href="<?php echo site_url('/forro-de-madeira/') ?>">Forro de Madeira</a></li>
        <li><a title="Ir para Forro de DryWall" class="transitioned-basic" href="<?php echo site_url('/forro-de-drywall/') ?>">Forro de DryWall</a></li>
        <li><a title="Ir para Forro de Gesso" class="transitioned-basic" href="<?php echo site_url('/forro-de-gesso/') ?>">Forro de Gesso</a></li>
	</ul>
</div>
<div class="footer-column one-fourth">
	<a  title="Ir para Persianas" class="footer-subtitle" href="<?= site_url('/category/persianas/') ?>">
		Persianas
	</a>
	<ul class="footer-list">
		<li><a title="Ir para Persianas Verticais" class="transitioned-basic" href="<?php echo site_url('/persianas-verticais/') ?>">Persianas Verticais</a></li>
        <li><a title="Ir para Persianas Horizantais" class="transitioned-basic" href="<?php echo site_url('/persianas-horizontais/') ?>">Persianas Horizantais</a></li>
        <li><a title="Ir para Persianas de Madeira" class="transitioned-basic" href="<?php echo site_url('/persianas-de-madeira/') ?>">Persianas de Madeira</a></li>	
        <li><a title="Ir para Persianas de Alumínio" class="transitioned-basic" href="<?php echo site_url('/persianas-de-aluminio/') ?>">Persianas de Alumínio</a></li>
	</ul>
	<a title="Ir para Pisos" class="footer-subtitle" href="<?= site_url('/category/pisos/') ?>">
		Pisos
	</a>
	<ul class="footer-list">
		<li><a title="Ir para Piso Laminados" class="transitioned-basic" href="<?php echo site_url('/pisos-laminados/') ?>">Pisos Laminados</a></li>
        <li><a title="Ir para Pisos Emborrachados" class="transitioned-basic" href="<?php echo site_url('/pisos-emborrachados/') ?>">Pisos Emborrachados</a></li>
        <li><a title="Ir para Pisos Vinilicos" class="transitioned-basic" href="<?php echo site_url('/pisos-vinilicos/') ?>">Pisos Vinilicos</a></li>
        <li><a title="Ir para Pisos Paviflex" class="transitioned-basic" href="<?php echo site_url('/pisos-paviflex/') ?>">Pisos Paviflex</a></li>
        <li><a title="Ir para Pisos Frios" class="transitioned-basic" href="<?php echo site_url('/pisos-frios/') ?>">Pisos Frios</a></li>
	</ul>
    <a title="Ir para Película de Proteção Solar" class="footer-subtitle"  href="<?php echo site_url('/pelicula-de-protecao-solar/') ?>">Película de Proteção Solar</a>
	<a title="Ir para Mezaninos Metálicos" class="footer-subtitle" href="<?php echo site_url('/mezaninos-metalicos/') ?>">Mezaninos Metálicos</a>
	<a title="Ir para Bate Maca"  class="footer-subtitle" href="<?php echo site_url('/bate-maca/') ?>">Bate Maca</a>
</div>