<div class="overlay">
	<div class="loader-wrapper">
		<?php get_template_part('inc/loader'); ?>
	</div>
	<div class="modal-wrapper">	
		<div class="modal-background modal-background-abstracted">
			<button class="close-modal transitioned-basic" aria-label="Fechar Modal">
				&times;
			</button>
			<img src="<?php bloginfo('template_url') ?>/images/common/modal-persona.png" aria-hidden="true" class="modal-persona call">
			<img src="<?php bloginfo('template_url') ?>/images/common/modal-persona2.png" aria-hidden="true" class="modal-persona work">
			<?php 
			get_template_part('inc/we-call'); 
			get_template_part('inc/work-with-us');
			get_template_part('inc/budget-form');
			?>
		</div>
	</div>
</div>