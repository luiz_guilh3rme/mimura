<div class="still">
	<div class="bgstripe-opaque">	
	</div>
	<div class="center-content cleared">	
		<div class="skewed-phone dib">
			<div class="unskew">
				<i class="fa fa-phone" aria-hidden="true"></i>
			</div>
		</div>
		<span class="generic-text white dib">
            <a href="tel:1139812093" itemprop="telephone" target="_BLANK" title="Ligue para nós!">11 3981.2093</a>
		</span>
		<div class="skewed-whatsapp dib">	
			<div class="unskew">
				<i class="fa fa-whatsapp" aria-hidden="true"></i>
			</div>
		</div>
        <span class="generic-text white dib">
        	<a href="wpp" onclick="ga('gtag_UA_121112366_1.send', 'event','click','Whatsapp', 'Header')" target="_BLANK" title="Fale conosco no Whatsapp">11 94703.1509</a></span>	
		<a href="<?php echo site_url('/'); ?>" class="triangle-wrapper" title="Home">	
			<div class="triangle"></div>
			<img src="<?php bloginfo('template_url'); ?>/images/common/logo.png" class="logo" alt="Logotipo Mimura" title="Logotipo Mimura">
		</a>
		<button class="btn-default full-red cta-we-call transitioned-basic open-modal"
		onclick="ga('gtag_UA_121112366_1.send', 'event', 'click', 'Formulário', 'Nós te Ligamos - Header')"
		 data-instance=".we-call-form" title="Nós te Ligamos">	
			<div class="unskew">
				NÓS TE LIGAMOS
			</div class="unskew">
		</button>
	</div>
</div>