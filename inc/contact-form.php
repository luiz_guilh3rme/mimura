	<section class="contact" id="contact-section">
		<?php if ( !is_page('trabalhe-conosco') ) { ?>
			<div class="prop transparent left shadowed" aria-hidden="true">
				<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/02.jpg);"></div>
			</div>
		<?php } ?>
		<div class="prop solid red right" aria-hidden="true"></div>
		<div class="prop transparent right shadowed">
			<div class="background" style="background-image: url(<?php bloginfo('template_url') ?>/images/props/03.jpg);"></div>
		</div>
		<div class="center-content">	
			<h2 class="section-title red has-shaded-copy default-margin" data-shade="ENTRE EM CONTATO">
				ENTRE EM <br> CONTATO
			</h2 >
			<p class="generic-text gray is-paragraph">
				Disponibilizamos um canal exclusivo para atendimento de nossos clientes. Veja como é fácil chegar em nossa matriz, ou se preferir utilize um outro canal de contato abaixo.
			</p>
			<div class="contact-information cleared">
				<div class="contact-information-instance one-third">
					<p class="contact-information-title">
						<span>Tem alguma dúvida?</span>
						Entre em Contato
					</p>
					<div class="icon-wrapper dib">
						<i class="fa fa-phone"></i>
					</div>
					<div class="information-wrapper dib">
						<p class="contact-details">
							<a href="tel:1139812093" title="Ligue para nós!" target="_BLANK" class="transitioned-basic" itemprop="telephone">11 3981.2093</a> <br> 
							<a href="tel:1139812235" title="Ligue para nós!" target="_BLANK" class="transitioned-basic" itemprop="telephone">11 3981.2235</a> <br>
							<a href="tel:1139857736" title="Ligue para nós!" target="_BLANK" class="transitioned-basic" itemprop="telephone">11 3985.7736</a>
						</p>
					</div>
				</div>
				<div class="contact-information-instance one-third">
					<p class="contact-information-title">
						<span>Se preferir, mande um</span>
						Whatsapp
					</p>
					<div class="icon-wrapper dib">
						<i class="fa fa-whatsapp"></i>
					</div>
					<div class="information-wrapper dib">
						<p class="contact-details">
							<a target="_BLANK" onclick="ga('gtag_UA_121112366_1.send', 'event','click','Whatsapp', 'Form de Contato')"
							href="wpp" title="Fale conosco pelo Whatsapp">11 94703.1509</a>
						</p>
					</div>
				</div>
				<div class="contact-information-instance one-third">
					<p class="contact-information-title">
						<span>Ou envie um</span>
						E-mail
					</p>
					<div class="icon-wrapper dib">
						<i class="fa fa-envelope-o"></i>
					</div>
					<div class="information-wrapper dib">
						<p class="contact-details">
							<a target="_BLANK" 
							onclick="ga('gtag_UA_121112366_1.send', 'event','click','Email', 'Form de Contato')"
							title="Nos envie um e-mail!" href="mailto:mimura@mimura.com.br">	
								mimura@mimura.com.br
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="main-contact-form">
				<?= do_shortcode( '[contact-form-7 id="62" title="Contato"]' );  ?>
			</div>
		</div>
	</section>