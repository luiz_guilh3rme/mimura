<div class="work-with-us-form">
	<fieldset>
		<legend>Preencha os campos abaixo e entraremos em contato o mais breve possível</legend>
		<label class="field-wrapper full">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper full">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper one-third-child">
			<input type="text" class="field shadowed">
		</label>
		<label class="field-wrapper full file">
			<input type="text" class="field shadowed hidden-file">
			<div class="field">	
				<div class="annex"></div>
				<span>Anexar Currículo</span>
			</div>
		</label>
		<label class="field-wrapper full">
			<div class="recaptcha"></div>
		</label>
		<button class="modal-submit transitioned-basic">
			ENVIAR
		</button>
	</fieldset>
</div>