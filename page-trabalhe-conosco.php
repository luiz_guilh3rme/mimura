<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/02.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			TRABALHE<br> CONOSCO
		</h1>
		<p class="generic-text white slightly-bigger">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
		</p>
	</div>
</div>
<main class="structure">
	<div class="work-props">	
		<div class="prop solid red" aria-hidden="true"></div>
		<div class="prop solid black shadowed" aria-hidden="true"></div>
	</div>
	<div class="center-content cleared">	
		<div class="workplace-spots cleared becomes-carousel-on-mobile">
			<?php 
			$args = array(
				'posts_per_page' => 12, 
				'post_type' => 'Vagas',
			);
			$query = new WP_Query( $args ); 
			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					?> 
					<div class="workplace-spot-instance one-third">
						<p class="workplace-spot-title">
							<?php the_title(); ?>
						</p>
						<p class="workplace-spot-date">
							<i class="fa fa-calendar"></i>
							<span class="dib generic-text gray smallest"><?php the_time('d, F, Y') ?></span>
						</p>
						<div class="image-wrapper">
							<?php 
							if (get_the_post_thumbnail()) {
								?>
								<img src="<?php echo get_the_post_thumbnail_url('', 'workplace-spot'); ?>"
								alt="<?php echo get_the_post_thumbnail_caption(); ?>" 
								title="<?php echo get_the_post_thumbnail_caption(); ?>" 
								class="post-thumbnail">
								<?php
							} 
							else {
								?>
								<img src="http://placehold.it/400x220" alt="Imagem base cinza">
								<?php 
							}
							?>
						</div>
						<a href="<?php the_permalink(); ?>" class="go-to-workplace-spot btn-default full-red transitioned-basic shaded">SAIBA MAIS</a>
					</div>
					<?php 
				}
			}
			?>
		</div>
	</div>
	<?php get_template_part('inc/contact-form'); ?>
	<?php get_template_part('inc/map'); ?>
</main>
<?php get_footer(); ?>