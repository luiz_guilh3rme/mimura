<?php 
	// Template Name: TyPage - Obrigado 
?>
<?php get_header(); ?>
<div class="breadcrumb has-black-border-bottom" 
style="background-image: url('<?php bloginfo('template_url') ?>/images/breadcrumbs/01.jpg');">
	<div class="center-content">
		<h1 class="section-title white smaller-margin">
			Obrigado Pelo Contato!
		</h1>
		<p class="generic-text white slightly-bigger">
			Em breve entraremos em contato! 
		</p>
        <a style="width:22.5%; margin-top: 10px;" href="<?= site_url('/') ?>" class="go-to-blog-post btn-default full-red transitioned-basic shaded" title="Voltar para Home">VOLTAR PARA HOME</a>
	</div>
</div>
<?php get_template_part('inc/blog-preview'); ?>
<?php get_template_part('inc/contact-form'); ?>
<?php get_template_part('inc/map'); ?>
<?php get_footer(); ?>